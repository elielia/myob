import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PayslipService, TaxRate, taxRates} from '../../services/payslip.service';
import {Utils} from '../../Utils';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{

  payslipForm: FormGroup;
  generatedPayslip;
  paidSuccess: boolean = false;
  payError: boolean = false;

  constructor(private payslipService: PayslipService, private router: Router){}

  ngOnInit(): void {

    this.payslipForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      salary: new FormControl('', [Validators.required, Validators.min(0)]),
      superRate: new FormControl('', [Validators.required, Validators.min(0)]),
    })

  }

  submit(){

    if (this.payslipForm.invalid){
      Utils.markFormGroupTouched(this.payslipForm);
      return;
    }

    this.generatedPayslip = this.generatePayslip(this.payslipForm.value);

  }

  reset(){
    this.generatedPayslip = null;
    this.paidSuccess = false;
    this.payError = null;
  }

  payPayslip(){

    this.payError = null;
    this.paidSuccess = false;

    this.payslipService.create(this.generatedPayslip).subscribe(
      _ => {
        this.paidSuccess = true;
      },
      err => {
        this.payError = true;
      })

  }

  private generatePayslip(formValue){

    let superAsPercentage = formValue.superRate / 100;
    let annualIncome = formValue.salary;
    let grossIncome = this.calculateGrossIncome(annualIncome);
    let incomeTax = this.calculateTax(annualIncome);
    let netIncome = this.calculateNetIncome(grossIncome, incomeTax);
    let superannuation = this.calculateSuper(grossIncome, superAsPercentage);
    let pay = this.calculatePay(netIncome, superannuation);

    return {
      firstName: formValue.firstName,
      lastName: formValue.lastName,
      date: new Date(),
      annualIncome,
      grossIncome,
      incomeTax,
      netIncome,
      superannuation,
      pay
    }
  }

  private calculateGrossIncome(salary: number){
    return Math.round(salary / 12);
  }

  private calculateTax(salary: number){
    return Math.round(taxRates.map(taxRate => this.calculateTaxForLevel(taxRate, salary)).reduce((a, b) => a + b, 0) / 12);
  }

  private calculateNetIncome(grossIncome: number, tax: number){
    return Math.round(grossIncome - tax);
  }

  private calculatePay(netIncome: number, superAnnuation: number){
    return Math.round(netIncome - superAnnuation);
  }

  private calculateSuper(income: number, superPercentage: number){
    return Math.round(income * superPercentage);
  }

  private calculateTaxForLevel(taxRate: TaxRate, income: number){

    if (income < taxRate.lowerBound) return 0;

    let normalizedUpperBound = Math.min(income, taxRate.upperBound);

    return (normalizedUpperBound - taxRate.lowerBound) * taxRate.taxPercentage;

  }
}

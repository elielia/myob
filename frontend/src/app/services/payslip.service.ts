import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PayslipService {

  constructor(private http : HttpClient) { }

  create(dto: UserWageInfo){
    return this.http.post("/api/tax/createPayslip", dto, {responseType: 'text'});
  }


}

export interface Payslip{
  id: string;
  date: Date;
  frequency: string;
  annualIncome: number;
  grossIncome: number;
  taxIncome: number;
  netIncome: number;
  superannuation: number;
  pay: number;
}

export interface UserWageInfo{
  firstName: string;
  lastName: string;
  salary: number;
  superRate: number;
}

export interface TaxRate{
  lowerBound: number;
  upperBound: number;
  taxPercentage: number;
}

export const taxRates: TaxRate[] = [
  {
    lowerBound: 0,
    upperBound: 18200,
    taxPercentage: 0
  },
  {
    lowerBound: 18200,
    upperBound: 37000,
    taxPercentage: 0.19
  },
  {
    lowerBound: 37000,
    upperBound: 80000,
    taxPercentage: 0.325
  },
  {
    lowerBound: 80000,
    upperBound: 180000,
    taxPercentage: 0.37
  },
  {
    lowerBound: 180000,
    upperBound: Number.MAX_SAFE_INTEGER,
    taxPercentage: 0.45
  }
];

# Coding Challenge - Myob

A payslip calculator for a coding challenge by Myob.

## How to run

An instance of the program is is up and running on a Google Cloud Platform and available at http://35.238.56.126:8080

For running it locally you would be rquired to install Docker and docker-compose. Then, follow these steps:

1. clone this repo

2. run ```cd myob```

3. run ```docker-compose pull```

4. run ```docker-compose up -d```

Then the application will be available at ```localhost:8080```


## Tech Stack I used


### Backend

Backend is built using Java and Spring Boot framework and containerized with Docker.

I used Spring because it was easy and fast to initialize with Spring Initializr.

MongoDB is used as the database for storing the Payrolls (Docker container).

Mongo is going very well with Spring and is super easy and fast to integrate with. 

There are no schemas and almost nothing to configure besides its URI. 

### Frontend


Front end is built with Angular 7 and bootstrap for styling.

I used these because I am familiar with them and are used by me daily.


## Assumptions I made

1. Input validation exists but is not strict as its only for internal use.


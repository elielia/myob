package com.myob.demo;

import java.util.Calendar;
import java.util.Date;

public class Utils {


    public static int getMonthOfDate(Date date){

        Calendar c = Calendar.getInstance();

        c.setTime(date);

        return c.get(Calendar.MONTH);

    }


}

package com.myob.demo.services;

import com.myob.demo.model.Payslip;
public interface PayslipService {

    Payslip save(Payslip payslip);

}

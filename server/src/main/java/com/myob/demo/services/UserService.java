package com.myob.demo.services;

import com.myob.demo.model.User;

public interface UserService {

    User create(String firstName, String lastName);

    User update(User user);

    User getUser(String firstName, String lastName);

}

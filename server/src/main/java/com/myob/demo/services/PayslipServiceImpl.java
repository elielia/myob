package com.myob.demo.services;

import com.myob.demo.model.Payslip;
import com.myob.demo.repositories.PayslipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PayslipServiceImpl implements PayslipService {

    @Autowired
    PayslipRepository payslipRepository;

    @Override
    public Payslip save(Payslip payslip) {

        return payslipRepository.save(payslip);

    }
}

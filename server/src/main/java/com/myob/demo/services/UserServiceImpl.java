package com.myob.demo.services;

import com.myob.demo.repositories.UserRepository;
import com.myob.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;


    @Override
    public User create(String firstName, String lastName) {

        User user = new User(firstName, lastName);

        return userRepository.insert(user);

    }

    @Override
    public User update(User user) {

        return this.userRepository.save(user);

    }

    @Override
    public User getUser(String firstName, String lastName) {

        return userRepository.findByFirstNameAndLastName(firstName, lastName);

    }
}

package com.myob.demo.repositories;

import com.myob.demo.model.Payslip;
import com.myob.demo.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PayslipRepository extends MongoRepository<Payslip, String> { }

package com.myob.demo.repositories;

import com.myob.demo.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    public User findByFirstNameAndLastName(String firstName, String lastName);

}

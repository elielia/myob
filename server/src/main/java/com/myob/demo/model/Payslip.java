package com.myob.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Document(collection = "payslips")
public class Payslip {

    @Id
    private String id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private Date date;
    private Payslip.PayFrequency payFrequency = PayFrequency.Monthly;
    @Min(0)
    private double annualIncome;
    @Min(0)
    private double grossIncome;
    @Min(0)
    private double incomeTax;
    @Min(0)
    private double netIncome;
    @Min(0)
    private double superannuation;
    @Min(0)
    private double pay;

    public Payslip() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Payslip.PayFrequency getPayFrequency() {
        return payFrequency;
    }

    public void setPayFrequency(Payslip.PayFrequency payFrequency) {
        this.payFrequency = payFrequency;
    }

    public double getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(double annualIncome) {
        this.annualIncome = annualIncome;
    }

    public double getGrossIncome() {
        return grossIncome;
    }

    public void setGrossIncome(double grossIncome) {
        this.grossIncome = grossIncome;
    }

    public double getIncomeTax() {
        return incomeTax;
    }

    public void setIncomeTax(double incomeTax) {
        this.incomeTax = incomeTax;
    }

    public double getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(double netIncome) {
        this.netIncome = netIncome;
    }

    public double getSuperannuation() {
        return superannuation;
    }

    public void setSuperannuation(double superannuation) {
        this.superannuation = superannuation;
    }

    public double getPay() {
        return pay;
    }

    public void setPay(double pay) {
        this.pay = pay;
    }

    public enum PayFrequency{
        Monthly
    }
}

package com.myob.demo.controllers;

import com.myob.demo.Utils;
import com.myob.demo.model.Payslip;
import com.myob.demo.services.PayslipService;
import com.myob.demo.services.UserService;
import com.myob.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;


@RestController
@RequestMapping("${api.prefix}/tax")
public class TaxController {

    @Autowired
    UserService userService;

    @Autowired
    PayslipService payslipService;

    @PostMapping("createPayslip")
    public String createPayslip(@RequestBody Payslip payslip) throws Exception {

        String firstName = payslip.getFirstName();

        String lastName = payslip.getLastName();

        User user = userService.getUser(firstName, lastName);

        if (user == null){
            user = userService.create(firstName, lastName);
        }

        int thisMonth = Utils.getMonthOfDate(new Date());

        long payslipCountOfMonth = user.getPayslips().stream().filter(p -> Utils.getMonthOfDate(p.getDate()) == thisMonth).count();

        if (payslipCountOfMonth > 0){
            throw new Exception("Payslip for this month already paid");
        }

        payslip = payslipService.save(payslip);

        user.addPayslip(payslip);

        userService.update(user);

        return payslip.getId();

    }


}
